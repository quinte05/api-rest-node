const { Router } = require('express');
const router = Router();

const movies = require('../sample.json');

router.get('/', (req, res) => {
    res.json(movies);
});

router.post('/', (req, res) => {
    const {titulo, description, autor, vistas} = req.body;
    if (titulo && description && autor && vistas){
        const id = movies.length + 1;
        const newMovie = {...req.body, id}
        movies.push(newMovie)
        res.json(movies);
    } else {
        res.status(500).json({error: 'Hubo un error, Faltan Datos'});
    }
    //console.log(req.body);
    
})

module.exports = router;